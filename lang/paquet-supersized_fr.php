<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: paquet-supersized
// Langue: fr

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A-Z
	'supersized_description' => 'Intégration dans SPIP du plugin «jQuery Supersized slideshow» (sous licence MIT)',
	'supersized_nom' => 'Supersized slideshow',
	'supersized_slogan' => 'Un slideshow en plein &#233;cran pour vos images'
);
?>
